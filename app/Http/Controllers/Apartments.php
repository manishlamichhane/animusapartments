<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Apartment;
use App\ApartmentCreator;
use App\Apartment_edit_token;
use DB;
use Mail;
use URL;

class Apartments extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id = null) {
        if ($id == null) {
            return Apartment::orderBy('id', 'asc')->get();
        } else {
            return $this->show($id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        $apartment = new Apartment;

        $apartment->location = $request->input('location');
        $apartment->length = $request->input('length');
        $apartment->breadth = $request->input('breadth');
        $apartment->type_id = $request->input('type_id');
        $apartment->rent = $request->input('rent');
        $apartment->details = $request->input('details');

        if($apartment->save()):

            $apartmentCreator = new ApartmentCreator;
            $apartmentCreator->apartment_id = $apartment->id;
            $apartmentCreator->creator_email = $request->input('email');
            $apartmentCreator->save();

            $randomToken = str_random(16);
            $editLink = URL::to('/').'/apartment/edit/'.$apartment->id.'?token='.$randomToken;

            $editToken = new Apartment_edit_token;
            $editToken->apartment_id = $apartment->id;
            $editToken->token = $randomToken;
            $editToken->save();

            $data = array('link' => $editLink);

            Mail::send('mail', $data, function($message) use($apartmentCreator) {
                $message->to($apartmentCreator->creator_email, 'Animus Apartment')->subject('Edit link for the apartment you created!');
                $message->from('manishlamichhane@gmail.com','Manish Lamichhane');
            });

            return 'Apartment  record successfully created with id ' . $apartment->id.'. Please check your email for edit link!';
        else:
            return 'There was an error saving the apartment record!';
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        /*return Apartment::find($id);*/

        return DB::table('apartments')
                    ->join('apartment_creators', 'apartments.id', '=', 'apartment_creators.apartment_id')
                    ->join('apartment_types', 'apartment_types.id', '=', 'apartments.type_id')
                    ->select('apartments.*', 'apartment_creators.creator_email','apartment_types.type')
                    ->where('apartments.id','=',$id)
                    ->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        $apartment = Apartment::find($id);

        $apartment->location = $request->input('location');
        $apartment->length = $request->input('length');
        $apartment->breadth = $request->input('breadth');
        $apartment->type_id = $request->input('type_id');
        $apartment->rent = $request->input('rent');
        $apartment->details = $request->input('details');
        $apartment->save();

        return "Sucess updating apartment #" . $apartment->id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
    	//since apartment_creator and apartment_edit_token have apartment's id as foreign key
    	//deleting the apartment will falsify the referential integrity
    	//so apartment_creator and apartment_edit_token have to be deleted first
        $apartment = Apartment::find($id);
        $apartment_creator = ApartmentCreator::where('apartment_id',$id);
        $apartment_creator->delete();
        $apartment_token = Apartment_edit_token::where('apartment_id',$id);
        $apartment_token->delete();
        $apartment->delete();

        return "Apartment record successfully deleted #" . $id;
    }
}
