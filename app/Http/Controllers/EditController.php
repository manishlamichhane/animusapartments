<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Apartment_edit_token;

class EditController extends Controller
{
    /**
     * Checks the token and loads the edit page if token is authenticated.
     *
     * @return Response
     */
    public function index($apartment_id, Request $request){

    	//check the token
   
    	$token = $request->input('token');

    	$count =  Apartment_edit_token::where('apartment_id','=',$apartment_id) 
    								->where('token','=',$token)
    								-> count();
    	if($count > 0):
    		//token validated
    		//user will be taken to edit page
    		$apartmentDetails = \App\Apartment::find($apartment_id);
    		$apartmentTypes = \App\ApartmentType::get();
    		return view('editApartment',array($apartmentDetails,$apartmentTypes));
    	endif;
    }
}
