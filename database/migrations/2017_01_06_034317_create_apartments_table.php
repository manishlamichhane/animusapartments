<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('apartments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('location');
            $table->string('length');
            $table->string('breadth');
            $table->integer('type_id');
            $table->string('rent');
            $table->text('details');
            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('apartment_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('apartments');
    }
}
