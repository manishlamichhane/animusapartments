# Introduction

This project is a test application developed as part of Job application assessment. It uses simple REST API developed using laravel 5.2 + PostgreSQL that is consumed by a frontend developed using html, css, javascript and angularjs (1.6)

# How to install

1. Clone the project

2. Create the postgressql database

3. update the dbname, dbusername and dbpassword in .env.example file with the new database details and rename this file to .env

4. Install dependencies using composer from the project root:
	composer install

5. Run database migrations (from project root):
	php artisan migrate

6. Seed the database:
	php artisan db:seed 

7. Generate key:
	php artisan key:generate

7. Run the server
	php artisan serve


#Notes

1.	Mail:
	gmail's mailserver is used to send emails. 
	Mandrill was planned but it turned out to be a paid service since april,2016.
	Please enable "allow less secure apps to login" option in google
	or use whichever mail server is prefered
	The .env file and the config/mail.php file is to be edited with the mail server details.
	If the mail doesnt appear in inbox, it should be in spam.

	##Also, the mail sending is a bit slower so, after hitting the save changes button in "Create Apartment" page, there is a delay of 2-3 seconds.

